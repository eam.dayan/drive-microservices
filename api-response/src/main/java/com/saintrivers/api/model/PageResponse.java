package com.saintrivers.api.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
@SuperBuilder
public class PageResponse<T> extends ApiResponse<T> {

    private Integer pageRequested;
    private Integer totalPages;
    private Integer totalElements;
    private Integer pageSize;

    public static <T> PageResponse.PageResponseBuilder<T,?,?> ok(T body){
        return PageResponse
                .<T>builder()
                .status("OK")
                .payload(body)
                .timestamp(LocalDateTime.now());
    }

}
