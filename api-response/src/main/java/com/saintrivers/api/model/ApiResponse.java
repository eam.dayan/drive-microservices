package com.saintrivers.api.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
@SuperBuilder
public class ApiResponse<T> {

    protected String message;
    protected String status;
    protected T payload;
    protected LocalDateTime timestamp = LocalDateTime.now();

    public static <T> ApiResponseBuilder<T,?,?> ok(T body){
        return ApiResponse
                .<T>builder()
                .status("OK")
                .payload(body)
                .timestamp(LocalDateTime.now());
    }

}
