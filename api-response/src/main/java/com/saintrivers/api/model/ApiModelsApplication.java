package com.saintrivers.api.model;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApiModelsApplication {

    public static void main(String[] args) {
        SpringApplication.run(ApiModelsApplication.class, args);
    }
}
