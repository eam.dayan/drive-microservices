package com.saintrivers.drive;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class ApplicationStart implements CommandLineRunner {

    @Value("${application.name}")
    private String appName;

    @Override
    public void run(String... args) throws Exception {
        log.info(appName);
    }
}
