create extension if not exists "uuid-ossp";
drop table if exists item_requests;
drop table if exists orders;
create table if not exists orders (
    id uuid primary key default uuid_generate_v4(),
    name varchar(50) not null,
    created_by uuid,
    created_at timestamp not null default now()
) ;
create table if not exists item_requests (
    id serial primary key ,
    order_id uuid references orders (id),
    item_name text not null,
    item_cost real default 0.0,
    is_cancelled bool default false,
    cancelled_by uuid,
    is_acquired bool default false,
    requested_at timestamp default now()
);