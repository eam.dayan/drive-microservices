package com.saintrivers.ordermanagement.feign;

import com.saintrivers.ordermanagement.config.security.oauth2.KeycloakUser;
import com.saintrivers.ordermanagement.config.security.oauth2.OAuthFeignConfig;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import reactivefeign.spring.config.ReactiveFeignClient;
import reactor.core.publisher.Flux;

import java.util.List;


@Component
@ReactiveFeignClient(
        name = "payment-client",
        url = "https://saintrivers.tech/auth/admin/realms/myrealm",
        configuration = OAuthFeignConfig.class)
public interface KeyCloakClient {

    @GetMapping("/users")
    Flux<KeycloakUser> getUsers();
}

