package com.saintrivers.ordermanagement;

import java.util.HashMap;
import java.util.Map;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.r2dbc.repository.config.EnableR2dbcRepositories;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import reactivefeign.spring.config.EnableReactiveFeignClients;

@SpringBootApplication
@EnableR2dbcRepositories
@RestController
@EnableReactiveFeignClients
public class OrderManagementApplication {

    public static void main(String[] args) {
        SpringApplication.run(OrderManagementApplication.class, args);
    }

    @GetMapping("/test")
    public Map<String,String> test(){
        Map<String, String> map = new HashMap<>();
        map.put("message", "this is working");
        return map;
    }
}
