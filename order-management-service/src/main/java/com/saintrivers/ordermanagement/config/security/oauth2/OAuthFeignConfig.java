package com.saintrivers.ordermanagement.config.security.oauth2;

import feign.RequestInterceptor;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpHeaders;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.client.*;
import org.springframework.security.oauth2.client.registration.ClientRegistration;
import org.springframework.security.oauth2.client.registration.ReactiveClientRegistrationRepository;
import reactor.core.publisher.Mono;

@Configuration
@RequiredArgsConstructor
@Slf4j
public class OAuthFeignConfig {

    public static final String CLIENT_REGISTRATION_ID = "keycloak";

    private final ReactiveClientRegistrationRepository clientRegistrationRepository;
    private final ReactiveOAuth2AuthorizedClientManager oAuth2AuthorizedClientManager;

    @Bean
    public RequestInterceptor requestInterceptor() {
        Mono<ClientRegistration> byRegistrationId = clientRegistrationRepository.findByRegistrationId(CLIENT_REGISTRATION_ID);

        OAuthClientCredentialsFeignManager clientCredentialsFeignManager =
                new OAuthClientCredentialsFeignManager(oAuth2AuthorizedClientManager, byRegistrationId.block());
        log.info(clientCredentialsFeignManager.getAccessToken());
        return requestTemplate -> requestTemplate.header(HttpHeaders.AUTHORIZATION, "Bearer " + clientCredentialsFeignManager.getAccessToken());



//        return requestTemplate -> {
//            Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
//
//            if (authentication != null && authentication.getDetails() instanceof OAuth2AuthenticationDetails) {
//                OAuth2AuthenticationDetails details = (OAuth2AuthenticationDetails) authentication.getDetails();
//                requestTemplate.header(HttpHeaders.AUTHORIZATION, String.format("Bearer %s", details.getTokenValue()));
//            }
//        };
    }

}
