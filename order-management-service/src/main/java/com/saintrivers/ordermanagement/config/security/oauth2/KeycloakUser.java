package com.saintrivers.ordermanagement.config.security.oauth2;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class KeycloakUser {

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    static class Access {
        private Boolean manageGroupMembership, view, mapRoles, impersonate, manage;
    }

    private String id;
    private Date createdTimestamp;
    private String username;
    private Boolean enabled;
    private Boolean totp;
    private Boolean emailVerified;
    private String firstName;
    private String lastName;
    private String email;
    private List<?> disabledCredentialTypes;
    private List<?> requiredActions;
    private Integer notBefore;
}
