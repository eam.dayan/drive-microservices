package com.saintrivers.ordermanagement.order.sevice;

import com.saintrivers.ordermanagement.order.domain.OrderRequest;
import com.saintrivers.ordermanagement.order.domain.OrderResponse;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface OrderService {

    Mono<OrderResponse> create(OrderRequest orderRequest);

    Flux<OrderResponse> findAll();

    Mono<OrderResponse> findById(String orderId);
}
