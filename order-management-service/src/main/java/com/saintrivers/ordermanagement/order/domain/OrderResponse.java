package com.saintrivers.ordermanagement.order.domain;

import com.saintrivers.ordermanagement.config.security.oauth2.KeycloakUser;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class OrderResponse {
    private UUID id;
    private String name;
    private LocalDateTime createdAt;
//    private OidcUser userId;

    private List<KeycloakUser> members;
}
