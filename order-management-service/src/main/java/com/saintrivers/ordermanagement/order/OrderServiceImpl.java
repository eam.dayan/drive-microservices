package com.saintrivers.ordermanagement.order;

import com.saintrivers.ordermanagement.feign.KeyCloakClient;
import com.saintrivers.ordermanagement.order.domain.Order;
import com.saintrivers.ordermanagement.order.domain.OrderRequest;
import com.saintrivers.ordermanagement.order.domain.OrderResponse;
import com.saintrivers.ordermanagement.order.sevice.OrderService;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.LocalDateTime;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class OrderServiceImpl implements OrderService {

    private final OrderRepository orderRepository;
    private final ModelMapper modelMapper;
//    private final KeyCloakClient keyCloakClient;

    private Flux<OrderResponse> toResponse(Flux<Order> orders) {
        return orders.map(res -> modelMapper.map(res, OrderResponse.class));
    }

    private Mono<OrderResponse> toResponse(Mono<Order> order) {
        return order.map(res -> modelMapper.map(res, OrderResponse.class));
    }


    @Override
    public Mono<OrderResponse> create(OrderRequest orderRequest) {
        Order order = modelMapper.map(orderRequest, Order.class);
        order.setCreatedAt(LocalDateTime.now());
        Mono<Order> result = orderRepository.save(order);
        return toResponse(result);
    }

    @Override
    public Flux<OrderResponse> findAll() {
        Flux<Order> result = orderRepository.findAll();
        return toResponse(result);
    }


    /**
     * todo: currently, the KeyCloakClient is not working
     * it's throwing an 401 error when trying to access keycloak via REST
     * @see com.saintrivers.ordermanagement.config.security.oauth2.OAuthFeignConfig OAuthFeignConfig
     */
    @Override
    public Mono<OrderResponse> findById(String orderId) {

//        Flux<KeycloakUser> users = keyCloakClient.getUsers();

        Mono<Order> result = orderRepository.findById(UUID.fromString(orderId));
        Mono<OrderResponse> orderResponseMono = toResponse(result);
//        return orderResponseMono.map(r -> {
//            users.subscribe(user -> r.getMembers().add(user));
//            return r;
//        });
        return orderResponseMono;
    }
}
