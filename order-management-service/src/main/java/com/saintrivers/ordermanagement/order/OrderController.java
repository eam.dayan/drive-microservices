package com.saintrivers.ordermanagement.order;


import com.saintrivers.ordermanagement.order.domain.OrderRequest;
import com.saintrivers.ordermanagement.order.sevice.OrderService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

//@FeignClient(
//        name = "payment-client",
//        url = "http://localhost:8081/resource-server-jwt",
//        configuration = OAuthFeignConfig.class)
//public interface PaymentClient {
//
//    @RequestMapping(value = "/payments", method = RequestMethod.GET)
//    List<Payment> getPayments();
//}



@RestController
@RequiredArgsConstructor
@RequestMapping("/api/v1/orders")
public class OrderController {

    private final OrderService orderService;

    @PostMapping
    public Mono<?> createOrder(@RequestBody OrderRequest orderRequest) {
        return orderService.create(orderRequest);
    }

    @GetMapping
    public Flux<?> getAllOrders() {
        return orderService.findAll();
    }

    @GetMapping("/{id}")
    public Mono<?> getAllOrders(@PathVariable("id") String orderId) {
        return orderService.findById(orderId);
    }
}
