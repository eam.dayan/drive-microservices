# Order Management Service

## References

> Main references, but there were problems that popped up that had to be handled differently. 

- https://www.baeldung.com/spring-cloud-feign-oauth-token
- https://github.com/eugenp/tutorials/tree/master/spring-cloud-modules/spring-cloud-openfeign

> Missing classes from the first two sources

- https://stackoverflow.com/questions/52338056/spring-oauth2-oidc-oauth2authorizedclientservice-is-not-registering-the-user-p

> There was a problem when Spring Boot couldn't find the custom keycloak configuration for an object in SecurityConfig.java called ClientRegistration.java

- https://docs.spring.io/spring-security/site/docs/5.2.3.RELEASE/reference/html5/#oauth2login-advanced-oidc-logout
- https://stackoverflow.com/questions/61017189/provider-id-must-be-specified-for-oauth2-client

> Another problem that came up was that I'm using Spring WebFlux. Therefore, we had to use the Reactive equivalent of the OAuth2 configuration classes.

- https://stackoverflow.com/questions/61800834/reactive-oauth2-with-spring-security-5-3-2-reactiveclientregistrationrepository
- https://stackoverflow.com/questions/64713348/clientregistrationrepository-bean-is-not-found
